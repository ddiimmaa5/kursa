﻿namespace curs
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBoxcp = new System.Windows.Forms.PictureBox();
            this.comboBoxvideo = new System.Windows.Forms.ComboBox();
            this.comboBoxCP = new System.Windows.Forms.ComboBox();
            this.comboBoxOZY = new System.Windows.Forms.ComboBox();
            this.labelcp = new System.Windows.Forms.Label();
            this.labelvideo = new System.Windows.Forms.Label();
            this.labelozy = new System.Windows.Forms.Label();
            this.buttonst = new System.Windows.Forms.Button();
            this.labelmoney = new System.Windows.Forms.Label();
            this.comboBoxmother = new System.Windows.Forms.ComboBox();
            this.labelmother = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonclearcp = new System.Windows.Forms.Button();
            this.buttonclearvideo = new System.Windows.Forms.Button();
            this.buttonclearozy = new System.Windows.Forms.Button();
            this.buttonclearmother = new System.Windows.Forms.Button();
            this.buttonclearall = new System.Windows.Forms.Button();
            this.pictureBoxvideo = new System.Windows.Forms.PictureBox();
            this.pictureBoxozy = new System.Windows.Forms.PictureBox();
            this.pictureBoxmother = new System.Windows.Forms.PictureBox();
            this.labelcp_info = new System.Windows.Forms.Label();
            this.labelvideo_info = new System.Windows.Forms.Label();
            this.labelozy_info = new System.Windows.Forms.Label();
            this.labelmother_info = new System.Windows.Forms.Label();
            this.labelcp_money = new System.Windows.Forms.Label();
            this.labelvideo_money = new System.Windows.Forms.Label();
            this.labelozy_money = new System.Windows.Forms.Label();
            this.labelmother_money = new System.Windows.Forms.Label();
            this.labelmoney_info = new System.Windows.Forms.Label();
            this.buttonblack = new System.Windows.Forms.Button();
            this.buttonlight = new System.Windows.Forms.Button();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxcp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxvideo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxozy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxmother)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxcp
            // 
            this.pictureBoxcp.Location = new System.Drawing.Point(308, 25);
            this.pictureBoxcp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBoxcp.Name = "pictureBoxcp";
            this.pictureBoxcp.Size = new System.Drawing.Size(101, 75);
            this.pictureBoxcp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxcp.TabIndex = 0;
            this.pictureBoxcp.TabStop = false;
            this.pictureBoxcp.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // comboBoxvideo
            // 
            this.comboBoxvideo.FormattingEnabled = true;
            this.comboBoxvideo.Items.AddRange(new object[] {
            "GTX 1080 TI",
            "GTX 1080",
            "GTX 1070 TI",
            "GTX 1070 ",
            "GTX 1060",
            "GTX 1050 TI",
            "GTX 1050",
            "RTX 2080 TI",
            "RTX 2080",
            "RTX 2070",
            "RX 590",
            "RX 580",
            "RX 570",
            "RX 5700XT",
            "RX 5700",
            "RX 5600XT",
            "RX 5500XT"});
            this.comboBoxvideo.Location = new System.Drawing.Point(101, 131);
            this.comboBoxvideo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboBoxvideo.Name = "comboBoxvideo";
            this.comboBoxvideo.Size = new System.Drawing.Size(181, 28);
            this.comboBoxvideo.TabIndex = 2;
            this.comboBoxvideo.SelectedIndexChanged += new System.EventHandler(this.comboBoxvideo_SelectedIndexChanged);
            // 
            // comboBoxCP
            // 
            this.comboBoxCP.FormattingEnabled = true;
            this.comboBoxCP.Items.AddRange(new object[] {
            "Intel Core I9 9900K",
            "Intel Core I7 10700K",
            "Intel Core I7 9700",
            "Intel Core I5 10400",
            "Intel Core I5 9600K",
            "Intel Core I5 9400",
            "Intel Core I3-10100",
            "Intel Core I3-9100",
            "Intel Core I3-8350K",
            "AMD Ryzen Threadripper 3990X",
            "AMD Ryzen 9 3950X",
            "AMD Ryzen 9 3900X",
            "AMD Ryzen 7 3800X",
            "AMD Ryzen 7 3700X",
            "AMD Ryzen 5 3600",
            "AMD Ryzen 5 2600",
            "AMD Ryzen 3 3300X",
            "AMD Ryzen 3 3200G",
            "AMD Athlon 3000G"});
            this.comboBoxCP.Location = new System.Drawing.Point(101, 49);
            this.comboBoxCP.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboBoxCP.Name = "comboBoxCP";
            this.comboBoxCP.Size = new System.Drawing.Size(181, 28);
            this.comboBoxCP.TabIndex = 3;
            this.comboBoxCP.SelectedIndexChanged += new System.EventHandler(this.comboBoxCP_SelectedIndexChanged);
            // 
            // comboBoxOZY
            // 
            this.comboBoxOZY.FormattingEnabled = true;
            this.comboBoxOZY.Items.AddRange(new object[] {
            "HyperX DDR4-3200 8192MB PC4-25600",
            "HyperX DDR4-3200 16384MB ",
            "HyperX DDR4-2400 8192MB PC4-19200 Fury",
            "Crucial DDR4-2666 8192MB",
            "HyperX DDR4-3200 8192MB PC4-25600"});
            this.comboBoxOZY.Location = new System.Drawing.Point(101, 211);
            this.comboBoxOZY.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboBoxOZY.Name = "comboBoxOZY";
            this.comboBoxOZY.Size = new System.Drawing.Size(181, 28);
            this.comboBoxOZY.TabIndex = 4;
            this.comboBoxOZY.SelectedIndexChanged += new System.EventHandler(this.comboBoxOZY_SelectedIndexChanged);
            // 
            // labelcp
            // 
            this.labelcp.AutoSize = true;
            this.labelcp.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelcp.Location = new System.Drawing.Point(693, 62);
            this.labelcp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelcp.Name = "labelcp";
            this.labelcp.Size = new System.Drawing.Size(45, 20);
            this.labelcp.TabIndex = 5;
            this.labelcp.Tag = "";
            this.labelcp.Text = "label1";
            // 
            // labelvideo
            // 
            this.labelvideo.AutoSize = true;
            this.labelvideo.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelvideo.Location = new System.Drawing.Point(693, 128);
            this.labelvideo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelvideo.Name = "labelvideo";
            this.labelvideo.Size = new System.Drawing.Size(45, 20);
            this.labelvideo.TabIndex = 6;
            this.labelvideo.Tag = "";
            this.labelvideo.Text = "label2";
            // 
            // labelozy
            // 
            this.labelozy.AutoSize = true;
            this.labelozy.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelozy.Location = new System.Drawing.Point(693, 186);
            this.labelozy.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelozy.Name = "labelozy";
            this.labelozy.Size = new System.Drawing.Size(45, 20);
            this.labelozy.TabIndex = 7;
            this.labelozy.Tag = "";
            this.labelozy.Text = "label3";
            // 
            // buttonst
            // 
            this.buttonst.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonst.Location = new System.Drawing.Point(101, 519);
            this.buttonst.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonst.Name = "buttonst";
            this.buttonst.Size = new System.Drawing.Size(204, 49);
            this.buttonst.TabIndex = 8;
            this.buttonst.Text = "Вибрати комплектацію";
            this.buttonst.UseVisualStyleBackColor = true;
            this.buttonst.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelmoney
            // 
            this.labelmoney.AutoSize = true;
            this.labelmoney.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelmoney.Location = new System.Drawing.Point(960, 306);
            this.labelmoney.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelmoney.Name = "labelmoney";
            this.labelmoney.Size = new System.Drawing.Size(45, 20);
            this.labelmoney.TabIndex = 9;
            this.labelmoney.Tag = "";
            this.labelmoney.Text = "label3";
            // 
            // comboBoxmother
            // 
            this.comboBoxmother.FormattingEnabled = true;
            this.comboBoxmother.Items.AddRange(new object[] {
            "Asus Prime TRX40-Pro",
            "Asus Prime X570-Pro",
            "Gigabyte X570 Gaming X",
            "ASRock X570 Phantom Gaming 4S",
            "Asus ROG Strix B450-F Gaming",
            "MSI B450 Gaming Plus Max",
            "Gigabyte B450 Aorus Elite",
            "Asus Prime A320M-K",
            "ASRock A320M Pro4-F",
            "Gigabyte Z390 Aorus Master",
            "Gigabyte Z390 Aorus Pro",
            "Asus ROG Strix Z390-F Gaming",
            "Asus Rog Strix B365-F Gaming",
            "ASRock B365 Pro4"});
            this.comboBoxmother.Location = new System.Drawing.Point(101, 302);
            this.comboBoxmother.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboBoxmother.Name = "comboBoxmother";
            this.comboBoxmother.Size = new System.Drawing.Size(181, 28);
            this.comboBoxmother.TabIndex = 10;
            this.comboBoxmother.SelectedIndexChanged += new System.EventHandler(this.comboBoxmother_SelectedIndexChanged);
            // 
            // labelmother
            // 
            this.labelmother.AutoSize = true;
            this.labelmother.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelmother.Location = new System.Drawing.Point(693, 246);
            this.labelmother.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelmother.Name = "labelmother";
            this.labelmother.Size = new System.Drawing.Size(45, 20);
            this.labelmother.TabIndex = 11;
            this.labelmother.Tag = "";
            this.labelmother.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(168, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "ЦП";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(143, 95);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "Відеокарта";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(168, 175);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "ОЗУ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(120, 268);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 20);
            this.label4.TabIndex = 15;
            this.label4.Text = "Материнська плата";
            // 
            // buttonclearcp
            // 
            this.buttonclearcp.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonclearcp.Location = new System.Drawing.Point(16, 49);
            this.buttonclearcp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonclearcp.Name = "buttonclearcp";
            this.buttonclearcp.Size = new System.Drawing.Size(69, 28);
            this.buttonclearcp.TabIndex = 16;
            this.buttonclearcp.Text = "Сброс";
            this.buttonclearcp.UseVisualStyleBackColor = true;
            this.buttonclearcp.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonclearvideo
            // 
            this.buttonclearvideo.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonclearvideo.Location = new System.Drawing.Point(16, 128);
            this.buttonclearvideo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonclearvideo.Name = "buttonclearvideo";
            this.buttonclearvideo.Size = new System.Drawing.Size(69, 29);
            this.buttonclearvideo.TabIndex = 17;
            this.buttonclearvideo.Text = "Сброс";
            this.buttonclearvideo.UseVisualStyleBackColor = true;
            this.buttonclearvideo.Click += new System.EventHandler(this.button3_Click);
            // 
            // buttonclearozy
            // 
            this.buttonclearozy.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonclearozy.Location = new System.Drawing.Point(16, 208);
            this.buttonclearozy.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonclearozy.Name = "buttonclearozy";
            this.buttonclearozy.Size = new System.Drawing.Size(69, 29);
            this.buttonclearozy.TabIndex = 18;
            this.buttonclearozy.Text = "Сброс";
            this.buttonclearozy.UseVisualStyleBackColor = true;
            this.buttonclearozy.Click += new System.EventHandler(this.button4_Click);
            // 
            // buttonclearmother
            // 
            this.buttonclearmother.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonclearmother.Location = new System.Drawing.Point(16, 298);
            this.buttonclearmother.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonclearmother.Name = "buttonclearmother";
            this.buttonclearmother.Size = new System.Drawing.Size(69, 29);
            this.buttonclearmother.TabIndex = 19;
            this.buttonclearmother.Text = "Сброс";
            this.buttonclearmother.UseVisualStyleBackColor = true;
            this.buttonclearmother.Click += new System.EventHandler(this.button5_Click);
            // 
            // buttonclearall
            // 
            this.buttonclearall.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonclearall.Location = new System.Drawing.Point(124, 353);
            this.buttonclearall.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonclearall.Name = "buttonclearall";
            this.buttonclearall.Size = new System.Drawing.Size(145, 29);
            this.buttonclearall.TabIndex = 20;
            this.buttonclearall.Text = "Скинути все";
            this.buttonclearall.UseVisualStyleBackColor = true;
            this.buttonclearall.Click += new System.EventHandler(this.button6_Click);
            // 
            // pictureBoxvideo
            // 
            this.pictureBoxvideo.Location = new System.Drawing.Point(308, 109);
            this.pictureBoxvideo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBoxvideo.Name = "pictureBoxvideo";
            this.pictureBoxvideo.Size = new System.Drawing.Size(101, 75);
            this.pictureBoxvideo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxvideo.TabIndex = 21;
            this.pictureBoxvideo.TabStop = false;
            // 
            // pictureBoxozy
            // 
            this.pictureBoxozy.Location = new System.Drawing.Point(308, 194);
            this.pictureBoxozy.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBoxozy.Name = "pictureBoxozy";
            this.pictureBoxozy.Size = new System.Drawing.Size(101, 75);
            this.pictureBoxozy.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxozy.TabIndex = 22;
            this.pictureBoxozy.TabStop = false;
            // 
            // pictureBoxmother
            // 
            this.pictureBoxmother.Location = new System.Drawing.Point(308, 278);
            this.pictureBoxmother.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBoxmother.Name = "pictureBoxmother";
            this.pictureBoxmother.Size = new System.Drawing.Size(101, 75);
            this.pictureBoxmother.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxmother.TabIndex = 23;
            this.pictureBoxmother.TabStop = false;
            // 
            // labelcp_info
            // 
            this.labelcp_info.AutoSize = true;
            this.labelcp_info.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelcp_info.Location = new System.Drawing.Point(639, 62);
            this.labelcp_info.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelcp_info.Name = "labelcp_info";
            this.labelcp_info.Size = new System.Drawing.Size(32, 20);
            this.labelcp_info.TabIndex = 24;
            this.labelcp_info.Tag = "";
            this.labelcp_info.Text = "ЦП:";
            // 
            // labelvideo_info
            // 
            this.labelvideo_info.AutoSize = true;
            this.labelvideo_info.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelvideo_info.Location = new System.Drawing.Point(584, 128);
            this.labelvideo_info.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelvideo_info.Name = "labelvideo_info";
            this.labelvideo_info.Size = new System.Drawing.Size(84, 20);
            this.labelvideo_info.TabIndex = 25;
            this.labelvideo_info.Tag = "";
            this.labelvideo_info.Text = "Відеокарта:";
            // 
            // labelozy_info
            // 
            this.labelozy_info.AutoSize = true;
            this.labelozy_info.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelozy_info.Location = new System.Drawing.Point(629, 186);
            this.labelozy_info.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelozy_info.Name = "labelozy_info";
            this.labelozy_info.Size = new System.Drawing.Size(39, 20);
            this.labelozy_info.TabIndex = 26;
            this.labelozy_info.Tag = "";
            this.labelozy_info.Text = "ОЗУ:";
            // 
            // labelmother_info
            // 
            this.labelmother_info.AutoSize = true;
            this.labelmother_info.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelmother_info.Location = new System.Drawing.Point(525, 246);
            this.labelmother_info.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelmother_info.Name = "labelmother_info";
            this.labelmother_info.Size = new System.Drawing.Size(137, 20);
            this.labelmother_info.TabIndex = 27;
            this.labelmother_info.Tag = "";
            this.labelmother_info.Text = "Материнська плата:";
            // 
            // labelcp_money
            // 
            this.labelcp_money.AutoSize = true;
            this.labelcp_money.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelcp_money.Location = new System.Drawing.Point(960, 62);
            this.labelcp_money.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelcp_money.Name = "labelcp_money";
            this.labelcp_money.Size = new System.Drawing.Size(45, 20);
            this.labelcp_money.TabIndex = 28;
            this.labelcp_money.Tag = "";
            this.labelcp_money.Text = "label5";
            // 
            // labelvideo_money
            // 
            this.labelvideo_money.AutoSize = true;
            this.labelvideo_money.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelvideo_money.Location = new System.Drawing.Point(960, 128);
            this.labelvideo_money.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelvideo_money.Name = "labelvideo_money";
            this.labelvideo_money.Size = new System.Drawing.Size(45, 20);
            this.labelvideo_money.TabIndex = 29;
            this.labelvideo_money.Tag = "";
            this.labelvideo_money.Text = "label6";
            // 
            // labelozy_money
            // 
            this.labelozy_money.AutoSize = true;
            this.labelozy_money.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelozy_money.Location = new System.Drawing.Point(960, 186);
            this.labelozy_money.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelozy_money.Name = "labelozy_money";
            this.labelozy_money.Size = new System.Drawing.Size(45, 20);
            this.labelozy_money.TabIndex = 30;
            this.labelozy_money.Tag = "";
            this.labelozy_money.Text = "label7";
            // 
            // labelmother_money
            // 
            this.labelmother_money.AutoSize = true;
            this.labelmother_money.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelmother_money.Location = new System.Drawing.Point(960, 246);
            this.labelmother_money.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelmother_money.Name = "labelmother_money";
            this.labelmother_money.Size = new System.Drawing.Size(45, 20);
            this.labelmother_money.TabIndex = 31;
            this.labelmother_money.Tag = "";
            this.labelmother_money.Text = "label8";
            // 
            // labelmoney_info
            // 
            this.labelmoney_info.AutoSize = true;
            this.labelmoney_info.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelmoney_info.Location = new System.Drawing.Point(894, 306);
            this.labelmoney_info.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelmoney_info.Name = "labelmoney_info";
            this.labelmoney_info.Size = new System.Drawing.Size(58, 20);
            this.labelmoney_info.TabIndex = 32;
            this.labelmoney_info.Tag = "";
            this.labelmoney_info.Text = "Всього:";
            // 
            // buttonblack
            // 
            this.buttonblack.Location = new System.Drawing.Point(886, 556);
            this.buttonblack.Name = "buttonblack";
            this.buttonblack.Size = new System.Drawing.Size(191, 33);
            this.buttonblack.TabIndex = 33;
            this.buttonblack.Text = "Вмикнути темний режим";
            this.buttonblack.UseVisualStyleBackColor = true;
            this.buttonblack.Click += new System.EventHandler(this.button7_Click);
            // 
            // buttonlight
            // 
            this.buttonlight.Location = new System.Drawing.Point(886, 556);
            this.buttonlight.Name = "buttonlight";
            this.buttonlight.Size = new System.Drawing.Size(191, 33);
            this.buttonlight.TabIndex = 34;
            this.buttonlight.Text = "Вимкнути темний режим";
            this.buttonlight.UseVisualStyleBackColor = true;
            this.buttonlight.Click += new System.EventHandler(this.buttonlight_Click);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 601);
            this.Controls.Add(this.buttonlight);
            this.Controls.Add(this.buttonblack);
            this.Controls.Add(this.labelmoney_info);
            this.Controls.Add(this.labelmother_money);
            this.Controls.Add(this.labelozy_money);
            this.Controls.Add(this.labelvideo_money);
            this.Controls.Add(this.labelcp_money);
            this.Controls.Add(this.labelmother_info);
            this.Controls.Add(this.labelozy_info);
            this.Controls.Add(this.labelvideo_info);
            this.Controls.Add(this.labelcp_info);
            this.Controls.Add(this.pictureBoxmother);
            this.Controls.Add(this.pictureBoxozy);
            this.Controls.Add(this.pictureBoxvideo);
            this.Controls.Add(this.buttonclearall);
            this.Controls.Add(this.buttonclearmother);
            this.Controls.Add(this.buttonclearozy);
            this.Controls.Add(this.buttonclearvideo);
            this.Controls.Add(this.buttonclearcp);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelmother);
            this.Controls.Add(this.comboBoxmother);
            this.Controls.Add(this.labelmoney);
            this.Controls.Add(this.buttonst);
            this.Controls.Add(this.labelozy);
            this.Controls.Add(this.labelvideo);
            this.Controls.Add(this.labelcp);
            this.Controls.Add(this.comboBoxOZY);
            this.Controls.Add(this.comboBoxCP);
            this.Controls.Add(this.comboBoxvideo);
            this.Controls.Add(this.pictureBoxcp);
            this.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximumSize = new System.Drawing.Size(1105, 640);
            this.MinimumSize = new System.Drawing.Size(1105, 640);
            this.Name = "Form1";
            this.Text = "Комплектація комп\'ютера";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxcp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxvideo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxozy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxmother)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxcp;
        private System.Windows.Forms.ComboBox comboBoxvideo;
        private System.Windows.Forms.ComboBox comboBoxCP;
        private System.Windows.Forms.ComboBox comboBoxOZY;
        private System.Windows.Forms.Label labelcp;
        private System.Windows.Forms.Label labelvideo;
        private System.Windows.Forms.Label labelozy;
        private System.Windows.Forms.Button buttonst;
        private System.Windows.Forms.Label labelmoney;
        private System.Windows.Forms.ComboBox comboBoxmother;
        private System.Windows.Forms.Label labelmother;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonclearcp;
        private System.Windows.Forms.Button buttonclearvideo;
        private System.Windows.Forms.Button buttonclearozy;
        private System.Windows.Forms.Button buttonclearmother;
        private System.Windows.Forms.Button buttonclearall;
        private System.Windows.Forms.PictureBox pictureBoxvideo;
        private System.Windows.Forms.PictureBox pictureBoxozy;
        private System.Windows.Forms.PictureBox pictureBoxmother;
        private System.Windows.Forms.Label labelcp_info;
        private System.Windows.Forms.Label labelvideo_info;
        private System.Windows.Forms.Label labelozy_info;
        private System.Windows.Forms.Label labelmother_info;
        private System.Windows.Forms.Label labelcp_money;
        private System.Windows.Forms.Label labelvideo_money;
        private System.Windows.Forms.Label labelozy_money;
        private System.Windows.Forms.Label labelmother_money;
        private System.Windows.Forms.Label labelmoney_info;
        private System.Windows.Forms.Button buttonblack;
        private System.Windows.Forms.Button buttonlight;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
    }
}

