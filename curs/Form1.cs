﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary;

namespace curs
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            labelcp.Text = null;
            labelvideo.Text = null;
            labelozy.Text = null;
            labelmother.Text = null;
            labelmoney.Text = null;
            labelcp_money.Text = null;
            labelvideo_money.Text = null;
            labelozy_money.Text = null;
            labelmother_money.Text = null;
            buttonlight.Visible = false;
        }

        private void comboBoxvideo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxvideo.SelectedIndex == 0)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/gtx1080ti.png");
            }
            if (comboBoxvideo.SelectedIndex == 1)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/gtx1080.png");
            }
            if (comboBoxvideo.SelectedIndex == 2)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/gtx1070ti.png");
            }
            if (comboBoxvideo.SelectedIndex == 3)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/gtx1070.png");
            }
            if (comboBoxvideo.SelectedIndex == 4)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/gtx1060.png");
            }
            if (comboBoxvideo.SelectedIndex == 5)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/gtx1050ti.png");
            }
            if (comboBoxvideo.SelectedIndex == 6)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/gtx1050.png");
            }
            if (comboBoxvideo.SelectedIndex == 7)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/rtx2080ti.png");
            }
            if (comboBoxvideo.SelectedIndex == 8)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/rtx2080.png");
            }
            if (comboBoxvideo.SelectedIndex == 9)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/rtx2070.png");
            }
            if (comboBoxvideo.SelectedIndex == 10)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/rx590.png");
            }
            if (comboBoxvideo.SelectedIndex == 11)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/rx580.png");
            }
            if (comboBoxvideo.SelectedIndex == 12)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/rx570.png");
            }
            if (comboBoxvideo.SelectedIndex == 13)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/rx5700xt.png");
            }
            if (comboBoxvideo.SelectedIndex == 14)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/rx5700.png");
            }
            if (comboBoxvideo.SelectedIndex == 15)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/rx5600xt.png");
            }
            if (comboBoxvideo.SelectedIndex == 16)
            {
                pictureBoxvideo.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/video/rx5500xt.png");
            }
        }

        private void comboBoxCP_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCP.SelectedIndex == 0)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/9900k.png");
            }
            if (comboBoxCP.SelectedIndex == 1)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/10700k.png");
            }
            if (comboBoxCP.SelectedIndex == 2)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/9700.png");
            }
            if (comboBoxCP.SelectedIndex == 3)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/10400.png");
            }
            if (comboBoxCP.SelectedIndex == 4)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/9600k.png");
            }
            if (comboBoxCP.SelectedIndex == 5)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/9400.png");
            }
            if (comboBoxCP.SelectedIndex == 6)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/10100.png");
            }
            if (comboBoxCP.SelectedIndex == 7)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/9100.png");
            }
            if (comboBoxCP.SelectedIndex == 8)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/8350k.png");
            }
            if (comboBoxCP.SelectedIndex == 9)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/3990x.png");
            }
            if (comboBoxCP.SelectedIndex == 10)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/3950x.png");
            }
            if (comboBoxCP.SelectedIndex == 11)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/3900x.png");
            }
            if (comboBoxCP.SelectedIndex == 12)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/3800x.png");
            }
            if (comboBoxCP.SelectedIndex == 13)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/3700x.png");
            }
            if (comboBoxCP.SelectedIndex == 14)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/3600.png");
            }
            if (comboBoxCP.SelectedIndex == 15)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/2600.png");
            }
            if (comboBoxCP.SelectedIndex == 16)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/3300x.png");
            }
            if (comboBoxCP.SelectedIndex == 17)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/3200g.png");
            }
            if (comboBoxCP.SelectedIndex == 18)
            {
                pictureBoxcp.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/cp/3000g.png");
            }
        }

        private void comboBoxOZY_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxOZY.SelectedIndex == 0)
            {
                pictureBoxozy.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/ozy/3200x8.png");
            }
            if (comboBoxOZY.SelectedIndex == 1)
            {
                pictureBoxozy.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/ozy/3200x16.png");
            }
            if (comboBoxOZY.SelectedIndex == 2)
            {
                pictureBoxozy.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/ozy/2400x8.png");
            }
            if (comboBoxOZY.SelectedIndex == 3)
            {
                pictureBoxozy.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/ozy/2666x8.png");
            }
            if (comboBoxOZY.SelectedIndex == 4)
            {
                pictureBoxozy.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/ozy/3200x8z1.png");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            videogra[] vg = new videogra[17];
            vg[0] = new videogra("GTX 1080Ti", "Nvidia", 17850);
            vg[1] = new videogra("GTX 1080", "Nvidia", 13199);
            vg[2] = new videogra("GTX 1070Ti", "Nvidia", 9713);
            vg[3] = new videogra("GTX 1070", "Nvidia", 8663);
            vg[4] = new videogra("GTX 1060", "Nvidia", 6578);
            vg[5] = new videogra("GTX 1050Ti", "Nvidia", 4699);
            vg[6] = new videogra("GTX 1050", "Nvidia", 4918);
            vg[7] = new videogra("RTX 2080Ti", "Nvidia", 42819);
            vg[8] = new videogra("RTX 2080", "Nvidia", 24646);
            vg[9] = new videogra("RTX 2070", "Nvidia", 15204);
            vg[10] = new videogra("RX 590", "AMD", 6083);
            vg[11] = new videogra("RX 580", "AMD", 5569);
            vg[12] = new videogra("RX 570", "AMD", 5025);
            vg[13] = new videogra("RX 5700XT", "AMD", 12505);
            vg[14] = new videogra("RX 5700", "AMD", 10725);
            vg[15] = new videogra("RX 5600XT", "AMD", 9578);
            vg[16] = new videogra("RX 5500XT", "AMD", 6695);
            CP[] cp = new CP[19];
            cp[0] = new CP("Intel Core I9 9900K", "Intel", 16850, "1151");
            cp[1] = new CP("Intel Core I7 10700K", "Intel", 13499, "1151");
            cp[2] = new CP("Intel Core I7 9700", "Intel", 11428, "1151");
            cp[3] = new CP("Intel Core I5 10400", "Intel", 6210, "1151");
            cp[4] = new CP("Intel Core I5 9600K", "Intel", 7339, "1151");
            cp[5] = new CP("Intel Core I5 9400", "Intel", 6990, "1151");
            cp[6] = new CP("Intel Core I3 10100", "Intel", 4190, "1151");
            cp[7] = new CP("Intel Core I3 9100", "Intel", 3719, "1151");
            cp[8] = new CP("Intel Core I3 8350K", "Intel", 4750, "1151");
            cp[9] = new CP("AMD Ryzen Theadripper 3990X", "AMD", 129039, "sTRX4");
            cp[10] = new CP("AMD Ryzen 9 3950X", "ADM", 23750, "AM4");
            cp[11] = new CP("AMD Ryzen 9 3900X", "AMD", 15455, "AM4");
            cp[12] = new CP("AMD Ryzen 7 3800X", "AMD", 10839, "AM4");
            cp[13] = new CP("AMD Ryzen 7 3700X", "AMD", 9499, "AM4");
            cp[14] = new CP("AMD Ryzen 5 3600", "AMD", 6249, "AM4");
            cp[15] = new CP("AMD Ryzen 5 2600", "AMD", 4540, "AM4");
            cp[16] = new CP("AMD Ryzen 3 3300X", "AMD", 3895, "AM4");
            cp[17] = new CP("AMD Ryzen 3 3200G", "AMD", 2799, "AM4");
            cp[18] = new CP("AMD Athlon 3000G", "AMD", 1630, "AM4");
            OZY[] oz = new OZY[5];
            oz[0] = new OZY("HyperX DDR4-3200 8192MB", 1285);
            oz[1] = new OZY("HyperX DDR4-3200 16384MB", 2590);
            oz[2] = new OZY("HyperX DDR4-2400 8190MB", 1463);
            oz[3] = new OZY("Crucial DDR4-2666 8192MB", 1075);
            oz[4] = new OZY("HyperX DDR4-3200 8192MB", 1399);
            motherboard[] mz = new motherboard[20];
            mz[0] = new motherboard("Asus Prime TRX40-Pro", "sTRX4", 14327, "AMD TRX40", "Asus");
            mz[1] = new motherboard("Asus Prime X570-Pro", "AM4", 7990, "AMD X570", "Asus");
            mz[2] = new motherboard("Gigabyte X570 Gaming X", "AM4", 5090, "AMD X570", "Gigabyte");
            mz[3] = new motherboard("ASRock X570 Phantom Gaming 4S", "AM4", 4615, "AMD X570", "ASRock");
            mz[4] = new motherboard("Asus ROG Strix B450-F Gaming", "AM4", 3960, "AMD B450", "Asus");
            mz[5] = new motherboard("MSI B450 Gaming Plus Max", "AM4", 3024, "AMD B450", "MSI");
            mz[6] = new motherboard("Gigabyte B450 Aorus Elite", "AM4", 3172, "AMD B450", "Gigabyte");
            mz[7] = new motherboard("Asus Prime A320M-K", "AM4", 1405, "AMD A320", "Asus");
            mz[8] = new motherboard("ASRock A320M Pro4-F", "AM4", 1966, "AMD A320", "ASRock");
            mz[9] = new motherboard("Gigabyte Z390 Aorus Master", "1151", 9450, "Intel Z390", "Gigabyte");
            mz[10] = new motherboard("Gigabyte Z390 Aorus Pro", "1151", 6350, "Intel Z390", "Gigabyte");
            mz[11] = new motherboard("Asus ROG Strix Z390-F Gaming", "1151", 5888, "Intel Z390", "Asus");
            mz[12] = new motherboard("Asus Rog Strix B365-F Gaming", "1151", 4320, "Intel B365", "Asus");
            mz[13] = new motherboard("ASRock B365 Pro4", "1151", 2709, "Intel B365", "ASRock");
            mz[14] = new motherboard("MSI MAG B365M MORTAR", "1151", 2685, "Intel B365", "MSI");
            double money = 0;
            if (comboBoxCP.SelectedIndex >= 0 && comboBoxvideo.SelectedIndex < 0 && comboBoxOZY.SelectedIndex < 0 && comboBoxmother.SelectedIndex < 0) 
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelcp_money.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelcp.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Name;
                money = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
            }
            if (comboBoxCP.SelectedIndex < 0 && comboBoxvideo.SelectedIndex >= 0 && comboBoxOZY.SelectedIndex < 0 && comboBoxmother.SelectedIndex < 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelvideo.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Name;
                labelvideo_money.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
            }
            if(comboBoxCP.SelectedIndex < 0 && comboBoxvideo.SelectedIndex < 0 && comboBoxOZY.SelectedIndex >= 0 && comboBoxmother.SelectedIndex < 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelozy.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Name;
                labelozy_money.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
            }
            if (comboBoxCP.SelectedIndex < 0 && comboBoxvideo.SelectedIndex < 0 && comboBoxOZY.SelectedIndex < 0 && comboBoxmother.SelectedIndex >= 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelmother.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Name;
                labelmother_money.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
            }
            if (comboBoxCP.SelectedIndex >= 0 && comboBoxvideo.SelectedIndex >= 0 && comboBoxOZY.SelectedIndex < 0 && comboBoxmother.SelectedIndex < 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelcp.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Name;
                labelcp_money.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelvideo.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Name;
                labelvideo_money.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money + cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
            }
            if (comboBoxCP.SelectedIndex >= 0 && comboBoxvideo.SelectedIndex < 0 && comboBoxOZY.SelectedIndex >= 0 && comboBoxmother.SelectedIndex < 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelcp.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Name;
                labelcp_money.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelozy.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Name;
                labelozy_money.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money + cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
            }
            if (comboBoxCP.SelectedIndex >= 0 && comboBoxvideo.SelectedIndex < 0 && comboBoxOZY.SelectedIndex < 0 && comboBoxmother.SelectedIndex >= 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelcp.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Name;
                labelcp_money.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelmother.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Name;
                labelmother_money.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money + cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
                if (cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Socet != mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Socet)
                {
                    labelmother.ForeColor = Color.Red;
                    labelcp.ForeColor = Color.Red;
                    MessageBox.Show("Непідходящий сокет, перевірте конфігурацію", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                }
                else
                {
                    if (BackColor == Color.Black)
                    {
                        labelmother.ForeColor = Color.White;
                        labelcp.ForeColor = Color.White;
                    }
                    else
                    {
                        labelmother.ForeColor = Color.Black;
                        labelcp.ForeColor = Color.Black;
                    }
                }
            }
            if (comboBoxCP.SelectedIndex < 0 && comboBoxvideo.SelectedIndex >= 0 && comboBoxOZY.SelectedIndex >= 0 && comboBoxmother.SelectedIndex < 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelvideo.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Name;
                labelvideo_money.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelozy.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Name;
                labelozy_money.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money + oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
            }
            if (comboBoxCP.SelectedIndex < 0 && comboBoxvideo.SelectedIndex >= 0 && comboBoxOZY.SelectedIndex < 0 && comboBoxmother.SelectedIndex >= 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelvideo.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Name;
                labelvideo_money.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelmother.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Name;
                labelmother_money.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money + vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
            }
            if (comboBoxCP.SelectedIndex < 0 && comboBoxvideo.SelectedIndex < 0 && comboBoxOZY.SelectedIndex >= 0 && comboBoxmother.SelectedIndex >= 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelozy.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Name;
                labelozy_money.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelmother.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Name;
                labelmother_money.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money + mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
            }
            if (comboBoxCP.SelectedIndex >= 0 && comboBoxvideo.SelectedIndex >= 0 && comboBoxOZY.SelectedIndex >= 0  && comboBoxmother.SelectedIndex < 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelcp.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Name;
                labelcp_money.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelvideo.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Name;
                labelvideo_money.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelozy.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Name;
                labelozy_money.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money + cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money + oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
            }
            if (comboBoxCP.SelectedIndex >= 0 && comboBoxvideo.SelectedIndex >= 0 && comboBoxOZY.SelectedIndex < 0 && comboBoxmother.SelectedIndex >= 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelcp.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Name;
                labelcp_money.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelvideo.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Name;
                labelvideo_money.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelmother.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Name;
                labelmother_money.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money + cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money + mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
                if (cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Socet != mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Socet)
                {
                    labelmother.ForeColor = Color.Red;
                    labelcp.ForeColor = Color.Red;
                    MessageBox.Show("Непідходящий сокет, перевірте конфігурацію", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (BackColor == Color.Black)
                    {
                        labelmother.ForeColor = Color.White;
                        labelcp.ForeColor = Color.White;
                    }
                    else
                    {
                        labelmother.ForeColor = Color.Black;
                        labelcp.ForeColor = Color.Black;
                    }
                }
            }
            if (comboBoxCP.SelectedIndex >= 0 && comboBoxvideo.SelectedIndex < 0 && comboBoxOZY.SelectedIndex >= 0 && comboBoxmother.SelectedIndex >= 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelcp.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Name;
                labelcp_money.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelozy.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Name;
                labelozy_money.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelmother.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Name;
                labelmother_money.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money + oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money + mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
                if (cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Socet != mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Socet)
                {
                    labelmother.ForeColor = Color.Red;
                    labelcp.ForeColor = Color.Red;
                    MessageBox.Show("Непідходящий сокет, перевірте конфігурацію", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (BackColor == Color.Black)
                    {
                        labelmother.ForeColor = Color.White;
                        labelcp.ForeColor = Color.White;
                    }
                    else
                    {
                        labelmother.ForeColor = Color.Black;
                        labelcp.ForeColor = Color.Black;
                    }
                }
            }
            if (comboBoxCP.SelectedIndex < 0 && comboBoxvideo.SelectedIndex >= 0 && comboBoxOZY.SelectedIndex >= 0 && comboBoxmother.SelectedIndex >= 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelvideo.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Name;
                labelvideo_money.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelozy.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Name;
                labelozy_money.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelmother.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Name;
                labelmother_money.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money + oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money + mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
            }
            if (comboBoxCP.SelectedIndex >= 0 && comboBoxvideo.SelectedIndex >= 0 && comboBoxOZY.SelectedIndex >= 0 && comboBoxmother.SelectedIndex >= 0)
            {
                labelcp.Text = null;
                labelvideo.Text = null;
                labelozy.Text = null;
                labelmother.Text = null;
                labelmoney.Text = null;
                labelcp_money.Text = null;
                labelmother_money.Text = null;
                labelozy_money.Text = null;
                labelvideo_money.Text = null;
                labelcp.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Name;
                labelcp_money.Text = cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelvideo.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Name;
                labelvideo_money.Text = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelozy.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Name;
                labelozy_money.Text = oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money.ToString() + " грн";
                labelmother.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Name;
                labelmother_money.Text = mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money.ToString() + " грн";
                money = vg[int.Parse(comboBoxvideo.SelectedIndex.ToString())].Money + cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Money + oz[int.Parse(comboBoxOZY.SelectedIndex.ToString())].Money + mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Money;
                labelmoney.Text = money.ToString() + " грн";
                if(cp[int.Parse(comboBoxCP.SelectedIndex.ToString())].Socet != mz[int.Parse(comboBoxmother.SelectedIndex.ToString())].Socet)
                {
                    labelmother.ForeColor = Color.Red;
                    labelcp.ForeColor = Color.Red;
                    MessageBox.Show("Непідходящий сокет, перевірте конфігурацію", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else{
                    if (BackColor == Color.Black)
                    {
                        labelmother.ForeColor = Color.White;
                        labelcp.ForeColor = Color.White;
                    }
                    else{
                        labelmother.ForeColor = Color.Black;
                        labelcp.ForeColor = Color.Black;
                    }
                }
            }
             
        }

        private void comboBoxmother_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBoxmother.SelectedIndex == 0)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/trx40.png");
            }
            if (comboBoxmother.SelectedIndex == 1)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/x570pro.png");
            }
            if (comboBoxmother.SelectedIndex == 2)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/x570x.png");
            }
            if (comboBoxmother.SelectedIndex == 3)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/x570s.png");
            }
            if (comboBoxmother.SelectedIndex == 4)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/b450f.png");
            }
            if (comboBoxmother.SelectedIndex == 5)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/b450mx.png");
            }
            if (comboBoxmother.SelectedIndex == 6)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/b450a.png");
            }
            if (comboBoxmother.SelectedIndex == 7)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/a320mk.png");
            }
            if (comboBoxmother.SelectedIndex == 8)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/a320m.png");
            }
            if (comboBoxmother.SelectedIndex == 9)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/z390am.png");
            }
            if (comboBoxmother.SelectedIndex == 10)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/z390ap.png");
            }
            if (comboBoxmother.SelectedIndex == 11)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/z390f.png");
            }
            if (comboBoxmother.SelectedIndex == 12)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/b365f.png");
            }
            if (comboBoxmother.SelectedIndex == 13)
            {
                pictureBoxmother.Image = Image.FromFile("C:/Users/куколд/source/repos/curs/motherboard/b365af.png");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            comboBoxCP.SelectedIndex = -1;
            if (pictureBoxcp.Image != null)
            {
                pictureBoxcp.Image.Dispose();
                pictureBoxcp.Image = null;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            comboBoxvideo.SelectedIndex = -1;
            if(pictureBoxvideo.Image != null)
            {
                pictureBoxvideo.Image.Dispose();
                pictureBoxvideo.Image = null;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            comboBoxOZY.SelectedIndex = -1;
            if(pictureBoxozy.Image != null)
            {
                pictureBoxozy.Image.Dispose();
                pictureBoxozy.Image = null;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            comboBoxmother.SelectedIndex = -1;
            if(pictureBoxmother.Image != null)
            {
                pictureBoxmother.Image.Dispose();
                pictureBoxmother.Image = null;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            comboBoxCP.SelectedIndex = -1;
            comboBoxvideo.SelectedIndex = -1;
            comboBoxOZY.SelectedIndex = -1;
            comboBoxmother.SelectedIndex = -1;
            if (pictureBoxcp.Image != null)
            {
                pictureBoxcp.Image.Dispose();
                pictureBoxcp.Image = null;
            }
            if (pictureBoxvideo.Image != null)
            {
                pictureBoxvideo.Image.Dispose();
                pictureBoxvideo.Image = null;
            }
            if (pictureBoxozy.Image != null)
            {
                pictureBoxozy.Image.Dispose();
                pictureBoxozy.Image = null;
            }
            if (pictureBoxmother.Image != null)
            {
                pictureBoxmother.Image.Dispose();
                pictureBoxmother.Image = null;
            }
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
        }

        private void button7_Click(object sender, EventArgs e)
        {
            BackColor = Color.Black;
            label1.ForeColor = Color.White;
            label2.ForeColor = Color.White;
            label3.ForeColor = Color.White;
            label4.ForeColor = Color.White;
            labelcp.ForeColor = Color.White;
            labelcp_info.ForeColor = Color.White;
            labelcp_money.ForeColor = Color.White;
            labelmoney.ForeColor = Color.White;
            labelmoney_info.ForeColor = Color.White;
            labelmother.ForeColor = Color.White;
            labelmother_info.ForeColor = Color.White;
            labelmother_money.ForeColor = Color.White;
            labelozy.ForeColor = Color.White;
            labelozy_info.ForeColor = Color.White;
            labelozy_money.ForeColor = Color.White;
            labelvideo.ForeColor = Color.White;
            labelvideo_info.ForeColor = Color.White;
            labelvideo_money.ForeColor = Color.White;
            comboBoxCP.BackColor = Color.FromArgb(37, 37, 37);
            comboBoxCP.ForeColor = Color.White;
            comboBoxvideo.BackColor = Color.FromArgb(37, 37, 37);
            comboBoxvideo.ForeColor = Color.White;
            comboBoxOZY.BackColor = Color.FromArgb(37, 37, 37); ;
            comboBoxOZY.ForeColor = Color.White;
            comboBoxmother.BackColor = Color.FromArgb(37, 37, 37); ;
            comboBoxmother.ForeColor = Color.White;
            buttonst.BackColor = Color.FromArgb(37, 37, 37);
            buttonst.ForeColor = Color.White;
            buttonclearcp.BackColor = Color.FromArgb(37, 37, 37);
            buttonclearcp.ForeColor = Color.White;
            buttonclearvideo.BackColor = Color.FromArgb(37, 37, 37);
            buttonclearvideo.ForeColor = Color.White;
            buttonclearozy.BackColor = Color.FromArgb(37, 37, 37);
            buttonclearozy.ForeColor = Color.White;
            buttonclearmother.BackColor = Color.FromArgb(37, 37, 37);
            buttonclearmother.ForeColor = Color.White;
            buttonclearall.BackColor = Color.FromArgb(37, 37, 37);
            buttonclearall.ForeColor = Color.White;
            buttonlight.BackColor = Color.FromArgb(37, 37, 37);
            buttonlight.ForeColor = Color.White;
            buttonblack.Visible = false;
            buttonlight.Visible = true;
        }

        private void buttonlight_Click(object sender, EventArgs e)
        {
            BackColor = Color.FromArgb(240, 240, 240);
            label1.ForeColor = Color.Black;
            label2.ForeColor = Color.Black;
            label3.ForeColor = Color.Black;
            label4.ForeColor = Color.Black;
            labelcp.ForeColor = Color.Black;
            labelcp_info.ForeColor = Color.Black;
            labelcp_money.ForeColor = Color.Black;
            labelmoney.ForeColor = Color.Black;
            labelmoney_info.ForeColor = Color.Black;
            labelmother.ForeColor = Color.Black;
            labelmother_info.ForeColor = Color.Black;
            labelmother_money.ForeColor = Color.Black;
            labelozy.ForeColor = Color.Black;
            labelozy_info.ForeColor = Color.Black;
            labelozy_money.ForeColor = Color.Black;
            labelvideo.ForeColor = Color.Black;
            labelvideo_info.ForeColor = Color.Black;
            labelvideo_money.ForeColor = Color.Black;
            comboBoxCP.BackColor = Color.White;
            comboBoxCP.ForeColor = Color.Black;
            comboBoxvideo.BackColor = Color.White;
            comboBoxvideo.ForeColor = Color.Black;
            comboBoxOZY.BackColor = Color.White;
            comboBoxOZY.ForeColor = Color.Black;
            comboBoxmother.BackColor = Color.White;
            comboBoxmother.ForeColor = Color.Black;
            buttonst.BackColor = Color.FromArgb(226, 226, 226);
            buttonst.ForeColor = Color.Black;
            buttonclearcp.BackColor = Color.FromArgb(226, 226, 226);
            buttonclearcp.ForeColor = Color.Black;
            buttonclearvideo.BackColor = Color.FromArgb(226, 226, 226);
            buttonclearvideo.ForeColor = Color.Black;
            buttonclearozy.BackColor = Color.FromArgb(226, 226, 226);
            buttonclearozy.ForeColor = Color.Black;
            buttonclearmother.BackColor = Color.FromArgb(226, 226, 226);
            buttonclearmother.ForeColor = Color.Black;
            buttonclearall.BackColor = Color.FromArgb(226, 226, 226);
            buttonclearall.ForeColor = Color.Black;
            buttonlight.BackColor = Color.FromArgb(226, 226, 226);
            buttonlight.ForeColor = Color.Black;
            buttonblack.Visible = true;
            buttonlight.Visible = false;
        }
    }
}
