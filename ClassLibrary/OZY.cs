﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary
{
    public class OZY
    {
        protected string name;
        protected double money;
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        public double Money
        {
            get
            {
                return money;
            }
            set
            {
                money = value;
            }
        }
        public OZY()
        {

        }
        public OZY(string Name, int Money)
        {
            name = Name;
            money = Money;
        }
    }
}
